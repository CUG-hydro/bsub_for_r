#!/bin/bash

#BSUB -J kongdd
#BSUB -o log.out
#BSUB -e log.out
#BSUB -n 20
#BSUB -R span[hosts=1]

#BSUB -u kongdd.sysu@gmail.com
#BSUB -N 

source /etc/profile.d/modules.sh

## 加载模块
# module load gcc/9.3.0
# module load netcdf-c/4.7.4-gcc930
export PATH="/share/opt/.conda/envs/qgis/bin:$PATH"
# ~/.profile works
# /share/opt/R/4.1.0/bin/R --file "~/github/bsub/hello.R"

Rscript hello.R
