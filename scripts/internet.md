```bash
192.168.101.1 c001 c001.hpc.cug 
192.168.101.2 c002 c002.hpc.cug 
192.168.101.3 c003 c003.hpc.cug 
192.168.101.4 c004 c004.hpc.cug 
192.168.101.5 c005 c005.hpc.cug 
192.168.101.6 c006 c006.hpc.cug 
192.168.101.7 c007 c007.hpc.cug 
192.168.101.8 c008 c008.hpc.cug 
192.168.101.9 c009 c009.hpc.cug

192.168.101.254 mn01 
```


## 网络正向代理

啥叫正向连接？就是client连上server，然后把server能访问的机器地址和端口（当然也包括server自己）镜像到client的端口上。

命令：

ssh -L [客户端IP或省略]:[客户端端口]:[服务器侧能访问的IP]:[服务器侧能访问的IP的端口] [登陆服务器的用户名@服务器IP] -p [服务器ssh服务端口（默认22）]

其中，客户端IP可以省略，省略的话就是127.0.0.1了，也就是说只能在客户端本地访问。服务器IP都可以用域名来代替。

举例说明：

你的IP是192.168.1.2，你可以ssh到某台服务器8.8.8.8，8.8.8.8可以访问8.8.4.4，你内网里还有一台机器可以访问你。

如果你想让内网里的另外一台电脑访问8.8.4.4的80端口的http服务，那么可以执行：

ssh -L 192.168.1.2:8080:8.8.4.4:80 test@8.8.8.8


也就是说，ssh到8.8.8.8上，然后让8.8.8.8把8.8.4.4的80端口映射到本地的8080端口上，而且和本地192.168.1.2这个IP绑定。

内网里的另外一台机器可以通过IE浏览器中输入http://192.168.1.2:8080查看8.8.4.4的网页。

当然，如果是其他服务，比如ftp、ssh、远程桌面也是可以的。不过，VPN貌似是不行的，可能是因为GRE协议无法通过。


## 实战

### ssh正向代理，端口映射
```bash
## 目标说明:
# 192.168.101.1可以连接到mn01;
# mn01可以连接到172.16.10.253;
# 通过mn01跳转使192.168.101.1连到mn01可以连接到172.16.10.253
ssh -L 192.168.101.1:1081:172.16.10.253:1081 root@mn01 # 在c001上执行
# ssh -R 172.16.10.253:1081:192.168.101.1:1081 root@c001 # 在mn01上执行

curl -x http://localhost:1081/ https://www.baidu.com
# curl -x http://172.16.10.253:1081/ https://www.baidu.com
# 测试通过!
```

### 设置网络代理
```bash
export http_proxy="http://192.168.101.1:1081/"
export https_proxy="http://192.168.101.1:1081/"
# 测试通过!
```

### 配置yum安装包镜像
```bash
# vim /
```

### ssh反向代理，将rstudio-server 8787端口对用户可见

```bash
## 目标说明:
# mn01可以连接到172.16.10.253;
# 192.168.101.1可以连接到mn01;
# 通过mn01跳转使192.168.101.1连到mn01可以连接到172.16.10.1
ssh -R 172.16.10.1:8788:192.168.101.1:8787 root@mn01

# curl -x http://192.168.101.1:1081/ https://www.baidu.com
# curl -x http://172.16.10.253:1081/ https://www.baidu.com
# 测试通过!
```

